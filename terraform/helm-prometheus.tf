locals {
  ### https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack
  ### https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack
  prometheus_helm_values = <<-EOF
    alertmanager:
      enabled: false
    grafana:
      enabled: false
    prometheusOperator:
      podLabels:
        sidecar.istio.io/inject: "false"
    prometheus:
      prometheusSpec:
        scrapeInterval: 30s
        additionalScrapeConfigs: |
          - job_name: 'istiod'
            kubernetes_sd_configs:
              - role: endpoints
                namespaces:
                  names:
                  - istio-system
            relabel_configs:
              - source_labels: [__meta_kubernetes_service_name, __meta_kubernetes_endpoint_port_name]
                action: keep
                regex: istiod;http-monitoring
          - job_name: 'envoy-stats'
            metrics_path: /stats/prometheus
            kubernetes_sd_configs:
              - role: pod

            relabel_configs:
              - source_labels: [__meta_kubernetes_pod_container_port_name]
                action: keep
                regex: '.*-envoy-prom'
  EOF
}

resource "helm_release" "prometheus" {
  name             = "prometheus"
  namespace        = "istio-system"
  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "kube-prometheus-stack"
  version          = "54.2.2"
  values           = [local.prometheus_helm_values]
}
