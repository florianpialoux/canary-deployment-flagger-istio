locals {
  ### https://github.com/fluxcd/flagger/tree/main/charts/flagger
  flagger_helm_values = <<-EOF
    meshProvider: istio
    metricsServer: "http://prometheus-kube-prometheus-prometheus.istio-system:9090/"
  EOF
}

resource "helm_release" "flagger" {
  name             = "flagger"
  namespace        = "istio-system"
  repository       = "https://flagger.app"
  chart            = "flagger"
  version          = "1.34.0"
  values           = [local.flagger_helm_values]
}
