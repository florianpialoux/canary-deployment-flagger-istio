locals {
  ### https://github.com/kiali/helm-charts/tree/master/kiali-server
  kiali_helm_values = <<-EOF
  auth:
    strategy: anonymous
  deployment:
    pod_labels:
      sidecar.istio.io/inject: "false"
  external_services:
    grafana:
      enabled: false
    istio:
      component_status:
        components:
        - app_label: "istiod"
          is_core: true
          is_proxy: false
        - app_label: "istio-ingressgateway"
          is_core: true
          is_proxy: true
          namespace: istio-ingress
        enabled: true
    prometheus:
      url: "http://prometheus-kube-prometheus-prometheus.istio-system:9090/"
    tracing:
      enabled: false

  EOF
}

resource "helm_release" "kiali" {
  name             = "kiali"
  namespace        = "istio-system"
  repository       = "https://kiali.org/helm-charts"
  chart            = "kiali-server"
  version          = "1.77.0"
  values           = [local.kiali_helm_values]
}
