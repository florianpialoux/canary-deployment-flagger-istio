# canary-deployment-flagger-istio

This repository is used to test and demo a canary deployment using [Istio](https://istio.io/) and [Flagger](https://fluxcd.io/flagger/).


## Information about the application

Bookinfo application has been reused from Istio example and contains 4x seperate microservices:

- `productpage`. The `productpage` microservice calls the details and reviews microservices to populate the page.
- `details`. The `details` microservice contains book information.
- `reviews`. The `reviews` microservice contains book reviews. It also calls the `ratings` microservice.
- `ratings`. The `ratings` microservice contains book ranking information that accompanies a book review.

There are 3 versions of the reviews microservice:

- Version v1 doesn’t call the `ratings` service.
- Version v2 calls the `ratings` service, and displays each rating as 1 to 5 **black** stars.
- Version v3 calls the `ratings` service, and displays each rating as 1 to 5 **red** stars.

The end-to-end architecture of the application is shown below.

![bookinfo](./docs/bookinfo_diagram.svg)

## How to provision this infrastructure

1. 
````
cd terraform
terraform init
terraform apply
````

2. `export GITLAB_TOKEN XXXX`
````
 flux bootstrap gitlab \
  --components source-controller,kustomize-controller \
  --deploy-token-auth \
  --owner=florianpialoux \
  --repository=canary-deployment-flagger-istio \
  --branch=main \
  --path=/flux
````
3. Open your browser and check http://$LB_EXT_IP/productpage
4. Update [spec.image](https://gitlab.com/florianpialoux/canary-deployment-flagger-istio/-/blob/main/flux/application/bookinfo_v1.yaml?ref_type=heads#L174) for reviews k8s deployment.
5. Flagger detects that the deployment revision changed and starts a new rollout:
`kubectl describe canaries`

You can also visualize Istio traffic with Kiali
`kubectl port-forward svc/kiali 20001 -n istio-system`

## Flagger progressive deployment
Let's upgrade our review microservice app by updating it to v3: `image: docker.io/istio/examples-bookinfo-reviews-v3:1.18.0`

kubectl -n test set image deployment/podinfo \
podinfod=ghcr.io/stefanprodan/podinfo:6.0.1

````
$ kubectl describe canaries

Events:
  Type     Reason  Age                   From     Message
  ----     ------  ----                  ----     -------
  Normal   Synced  2m15s (x4 over 146m)  flagger  New revision detected! Scaling up reviews.default
  Normal   Synced  2m (x3 over 146m)     flagger  Starting canary analysis for reviews.default
  Normal   Synced  2m (x3 over 146m)     flagger  Advance reviews.default canary weight 10
  Normal   Synced  105s (x3 over 145m)   flagger  Advance reviews.default canary weight 20
  Normal   Synced  90s (x3 over 145m)    flagger  Advance reviews.default canary weight 30
  Normal   Synced  75s (x3 over 145m)    flagger  Advance reviews.default canary weight 40
  Normal   Synced  60s (x3 over 145m)    flagger  Advance reviews.default canary weight 50
  Normal   Synced  45s (x2 over 61m)     flagger  Copying reviews.default template spec to reviews-primary.default
  Normal   Synced  30s (x2 over 61m)     flagger  Routing all traffic to primary
  Normal   Synced  15s (x5 over 144m)    flagger  (combined from similar events): Promotion completed! Scaling down reviews.default
````

## Flagger rollback
In this example we will deploy a bad revision of our review microservice app by modifying its k8s deployment with a bad image: `image: docker.io/istio/examples-bookinfo-reviews-v0`
````
$ kubectl describe canaries

Events:
  Type     Reason  Age                 From     Message
  ----     ------  ----                ----     -------
  Normal   Synced  77s (x3 over 140m)  flagger  New revision detected! Scaling up reviews.default
  Warning  Synced  32s (x3 over 62s)   flagger  canary deployment reviews.default not ready: waiting for rollout to finish: 0 of 1 (readyThreshold 100%) updated replicas are available
  Warning  Synced  17s                 flagger  Rolling back reviews.default progress deadline exceeded canary deployment reviews.default not ready: waiting for rollout to finish: 0 of 1 (readyThreshold 100%) updated replicas are available
  Warning  Synced  17s                 flagger  Canary failed! Scaling down reviews.default
````

## To do

- [ ] Research and test flagger-loadtester

## References
 
- https://istio.io/latest/docs/examples/bookinfo
- https://istio.io/latest/docs/concepts/traffic-management
- https://fluxcd.io/flagger/tutorials/istio-progressive-delivery